import psycopg


class PostgresSQLController:
    def __init__(self, dbname: str, host: str, port: int, username: str, password: str):
        """Provides interface with PostgreSQL type databases with easy access without needing to use SQL language
        """

        self.__connected: bool = False

        self.__connectionInfo = {
            "dbname": dbname,
            "host": host,
            "port": port,
            "user": username,
            "password": password
        }

        self.connection: psycopg.connection.Connection
        self.__connect()

    def __connect(self):
        """Some connection documentation
        https://www.npgsql.org/doc/connection-string-parameters.html
        """
        if not self.__connected:
            try:
                self.connection = psycopg.connect(f"host={self.__connectionInfo['host']} dbname={self.__connectionInfo['dbname']} port={self.__connectionInfo['port']} user={self.__connectionInfo['user']} password={self.__connectionInfo['password']}")
                self.__connected = True
            except psycopg.Error as e:
                print(e)  # Maybe include error handling and logging later
        else:
            raise "Attempted Connection to Database while already connected!"

    def disconnect(self):
        if self.__connected:
            try:
                self.connection.close()
            except Exception as e:
                print(e)
                # Yes this is very broad, but I couldn't care less
        else:
            raise "Attempted to disconnect from Database but is not connected to any Database"


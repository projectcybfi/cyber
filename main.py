from detoxify import Detoxify
from transformers import AutoModelForSequenceClassification, AutoTokenizer
from scipy.special import expit
from dotenv import load_dotenv
load_dotenv()
import psycopg
import os
from PostgresSQLController import PostgresSQLController

postgres_controller = PostgresSQLController(
    dbname=os.getenv('DB_NAME'),
    host=os.getenv("DB_HOST"),
    port=int(os.getenv("DB_PORT")),
    username=os.getenv("DB_USER"),
    password=os.getenv("DB_PASS"))


model = AutoModelForSequenceClassification.from_pretrained("cardiffnlp/tweet-topic-21-multi")
tokenizer = AutoTokenizer.from_pretrained("cardiffnlp/tweet-topic-21-multi")
class_mapping = model.config.id2label

interval = 0

while True:
    print(interval)
    messages = postgres_controller.connection.cursor() \
        .execute("SELECT * FROM messages ORDER BY id OFFSET %i FETCH NEXT 1000 ROWS ONLY;", interval) \
        .fetchall()


    if messages.__len__() == 0:
        break


    for message in messages:
        try:
            interval += 1

            results = Detoxify('unbiased').predict(message["message"])
            if (results["toxicity"] > 0.5):
                d = results
                d["toxicity"] = 0
                try:
                    postgres_controller.connection.cursor() \
                        .execute("UPDATE messages SET rating = %s WHERE id = %i", 
                                 max(d, key=lambda k: d[k]), interval)
                except psycopg.Error as e:
                    print("Error uploading...")
                    print(e)
                    postgres_controller.connection.rollback()
                    pass

                
            else:
                inputs = tokenizer(message["message"], return_tensors="pt")
                outputs = model(**inputs)
                scores = outputs[0][0].detach().numpy()
                scores = expit(scores)
                predictions = (scores >= 0.1) * 1
                prediction = {}
                for i in range(len(predictions)):
                    if predictions[i]:
                        prediction[class_mapping[i]] = scores[i]
                try:
                    postgres_controller.connection.cursor() \
                        .execute("UPDATE messages SET rating = %s WHERE id = %i", 
                                 max(prediction, key=lambda k: prediction[k]), interval)
                except psycopg.Error as e:
                    print("Error uploading...")
                    print(e)
                    postgres_controller.connection.rollback()
                    pass

            postgres_controller.connection.commit()
        except psycopg.Error as e:
            print("Error uploading...")
            print(e)
            postgres_controller.connection.rollback()
            pass

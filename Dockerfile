FROM python:3.10-alpine

WORKDIR /usr/src/app

RUN pip install transformers detoxify scipy psycopg[binary,pool] python-dotenv
COPY . .

CMD [ "python", "./main.py" ]
